TP01
![Screenshot_95](https://hackmd.io/_uploads/Hk15xg0Tp.png)

TP02
![Screenshot_96](https://hackmd.io/_uploads/HJI8-gA6a.png)

TP03
![Screenshot_98](https://hackmd.io/_uploads/BJQoge06a.png)


TP04: PLAYBOOK

stopvm.yml
---
- name: Arrêter les machines virtuelles
  hosts: all
  tasks:
    - name: Arrêter les machines virtuelles
      shell: sudo shutdown -h now
 
TP05: PLAYBOOK

restartvm.yml
---
- name: Redémarrer les nodes
- hosts: all
  tasks:
    - name: Redémarrer les nodes
      shell: sudo reboot
      become: yes
      ignore_errors: yes

TP06: PLAYBOOK

renamevm.yml
---
- name: Renommer les machines
  hosts: all
  tasks:
    - name: Renommer la machine
      command: sudo hostnamectl set-hostname nodes
      become: yes
     
TP07

![Screenshot_99](https://hackmd.io/_uploads/B1wnxeA6a.png)

TP08
![Screenshot_100](https://hackmd.io/_uploads/r16y-gRT6.png)

TP09
![Screenshot_101](https://hackmd.io/_uploads/SJdeZlRpa.png)

TP10
![Screenshot_102](https://hackmd.io/_uploads/H1TbWgA66.png)

TP11: PLAYBOOK (avant le ssh irréparable)
install.yml
---
- name: Install Nginx on Node-web
  hosts: 10.0.6.20
  tasks:
    - name: Install Nginx
      apt:
        name: nginx
        state: present

- name: Install MariaDB on Node-db
  hosts: 10.0.6.36
  tasks:
    - name: Install MariaDB
      apt:
        name: mariadb-server
        state: present
        
TP12: PLAYBOOK

modifierhtml.yml
---
- name: Modifier la page index.html sur la nodeweb
  hosts: 10.0.6.20
  become: true
  tasks:
    - name: Copier le nouveau fichier index.html sur la nodeweb
      copy:
        src: nouveauhtml.html
        dest: /var/www/html/index.html

TP13: PLAYBOOK

exclurejava.yml
---
- hosts: all
  become: true
  vars:
  yum_conf: /etc/apt/sources.list
  tasks:
    - name: S'assurer que Java est exclu
      lineinfile:
      path: "{{ yum_conf }}"
      regexp: '^Package:'
      line: 'Package: kernel* java*'